---

#########################################################################
### Vault settings
#########################################################################

# URL of the Vault server
# vault_server_base_url: "https://hostname.example.local:8200"

# the Vault token to use when requesting a SSL server certificate
# vault_server_token: "<place token here>"

# the Vault token to use when requesting a SSL client certificate
# vault_client_token: "<place token here>"


#########################################################################
### OSSEC server settings
#########################################################################

# name of the OSSEC rpm package
# latest_atomic_rpm: "atomic-release-1.0-21.el6.art.noarch.rpm"

# atomic repo url 
# atomic_repo_url: "https://www6.atomicorp.com/channels/atomic/centos/6/x86_64/RPMS/{{ latest_atomic_rpm }}"


#########################################################################
### splunk forwarder settings
#########################################################################

# the common name of the Splunk server's certificate
# splunk_indexer_cn: "splunkserver.example.local"

# list of splunk indexers to forward events to
# splunk_indexers:
#  - splunkserver.example.local:1514

# the tcp port splunk indexer is listening on
# splunk_indexer_port: 1514


#########################################################################
### monit settings
#########################################################################

# monit version
# monit_version: "5.17"

# url to download latest pre-compiled x86_64 monit binary
# monit_zip_url: "https://mmonit.com/monit/dist/binary/{{ monit_version }}/monit-{{ monit_version }}-linux-x64.tar.gz"

# sha256 checksum 
# monit_zip_hash: "sha256:9229091d7b812e71942c2ad6212e03a9fe5ad6e7a63a991232c90202b033ee75"

# monit will run its checks every X seconds
# monit_check_interval: 300

# set to true to enable email notifications 
# enable_monit_notifications: false

# set to the email receipient for monit notifications 
# monit_receipient: "root@localhost"

# set to the mail server to use for delivering notifications
# monit_smtp_server: 127.0.0.1

# set the port of the mail server
# monit_smtp_port: 25

# set to true if the smtp server uses tls
# monit_smtp_ssl: false


#########################################################################
### iptables settings
#########################################################################

# placeholder used in iptable comments for templating
# comment_regex_keyword: "tobereplaced"

# the log prefix to include in iptables log messages
# iptables_log_prefix: "PACKET DROPPED"


#########################################################################
### psad settings
#########################################################################

# the log level for the psad daemon
# psad_log_level: "LOG_NOTICE"

# the syslog facility for the psad daemon
# psad_log_facility: "LOG_LOCAL3"

# the iptables prefix that psad looks for in iptables log messages
# iptables_log_prefix: "PACKET DROPPED"


#########################################################################
### auditd settings
#########################################################################

# log level and facility for audisp
# auditd_log_level: LOG_NOTICE
# auditd_log_facility: LOG_LOCAL5

# set to True if using custom audit rules
# auditd_use_custom_rules: false
