#!/usr/bin/python
# -*- coding: utf-8 -*-

# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

#---- Documentation Start ----------------------------------------------------#

DOCUMENTATION = '''
---
module: generate_ssl_cert
version_added: 2.1
author: "Ali Ibrahim (@aeibrahim)"
requires: [ openssl ]
short_description: Generates self-signed RSA or ECDSA SSL certificates in PEM format.
description:
     - The M(generate_ssl_cert) module is a wrapper around the openssl req command. It can be used to generate RSA or ECDSA self-signed SSL certificates, certificate signing requests (CSRs), and their associated private keys on the remote server in PEM format. 
options:
  dest:
    description:
      - Remote absolute path where the generated certificate file should be created. If you specify an extension, it is stripped off and replaced with .crt if a self-signed certificate, .csr for CSRs, and .key for private keys. If the parent directories do not exist, they are automatically created as long as the user has sufficient privileges.  
    required: true
    default: null
    aliases: [ 'path', 'destfile' ]
  backup:
    description:
      - Creates backup files including the timestamp information so you can get the original files back if you somehow clobbered it incorrectly. It is assumed that the private key file has the same name as the certificate, but with an extension of .key. If set to C(no) and SSL files already exist, it will do nothing. 
    required: false
    default: "no"
    choices: [ "yes", "no" ]
  self_signed:
    description:
      - the default is C(yes) which will create a self-signed certificate and private key. C(no) will generate a CSR. 
    required: false
    default: "yes"
    choices: [ "yes", "no" ]
  encrypt:
    description:
      - the default is C(no) which will generate a private key that is not encrypted. C(yes) will generate an encrypted private key using the password provided by the I(password) option.  
    required: false
    default: "no"
    choices: [ "yes", "no" ]
  password:
    description:
      - Optionally provide a password to use to encrypt the generated private key. Does not log the provided password. If I(encrypt) is set to C(yes), you must specify a password.
    required: false
    default: null
  type:
    description:
      - the type of certificate to generate. If type is C(ecdsa), you must specify a I(curve) to use.
    required: false
    default: "rsa"
    choices: [ "rsa", "ecdsa" ]
  key_size:
    description:
      - the size of the RSA private key in bits. 
    required: false
    default: 2048
  curve:
    description:
      - the elliptic curve to use. This is required if certificate type is C(ecdsa).
    required: false
    default: null
  digest:
    description:
      - the message digest to use. 
    required: false
    default: null
    aliases: ['hash']
  days_valid:
    description:
      - the number of days the self-signed certificate should be valid for. 
    required: false
    default: 365
  cnf_file:
    description:
      - Remote absolute path to a custom openssl configuration file to use when generating certificates. The openssl.cnf should contain valid syntax. 
    required: false
    default: null
    aliases: ['config', 'config_file']
  extensions:
    description:
      - This option is used alongside I(cnf_file) to allow specifying alternative sections to use when generating certificates. If the specified section does not exist in the config file, it will cause the task to fail. 
    required: false
    default: null
    aliases: ['ext']
  country:
    description:
      - The country portion of the subject in the certificate. The two-letter ISO code for the country where your organization is located. 
    required: false
    default: null
    aliases: ['C']
  state:
    description:
      - The state, province or region portion of the subject in the certificate. This should not be abbreviated. 
    required: false
    default: null
    aliases: ['ST']
  city:
    description:
      - The city or town portion of the subject in the certificate. This should not be abbreviated. 
    required: false
    default: null
    aliases: ['L']
  org:
    description:
      - The organization portion of the subject in the certificate. Usually the legal incorporated name of a company and should include any suffixes such as Ltd., Inc., or Corp. 
    required: false
    default: null
    aliases: ['O']
  org_unit:
    description:
      - The organizational unit portion of the subject in the certificate. Usually the department name within the company. 
    required: false
    default: null
    aliases: ['OU']
  common_name:
    description:
      - The common name portion of the subject in the certificate. This is the fully qualified domain name of the server. 
    required: true
    default: null
    aliases: ['CN']
  email:
    description:
      - The email portion of the subject in the certificate. This should be a valid email address to contain the organization. Usually the email address of the certificate administrator or IT department. 
    required: false
    default: null
    aliases: ['email_address']
  owner:
    description:
      - the owner id of the certificate and private key files, after execution. 
    required: false
    default: null
  group:
    description:
      - the group id of the certificate and private key files, after execution. 
    required: false
    default: null
  mode:
    description:
      - the permissions of the certificate file, after execution. The permissions of the private key is always set to 0600. 
    required: false
    default: null
notes:
  - "This module depends on I(openssl), which needs to be installed on all target systems."
'''

EXAMPLES = '''
# generate a 2048-bit RSA self-signed SSL certificate
- generate_ssl_cert: path=/etc/server.crt CN=www.example.local

# generate a 4096-bit RSA self-signed certificate and encrypt the private key with a password
- generate_ssl_cert: path=/etc/server.csr CN=www.example.local email='security@example.local' encrypt=yes password='hunter123' key_size=4096

# generate an ECDSA self-signed certificate using an OpenSSL supported curve
- generate_ssl_cert: path=/etc/server.csr CN='www.example.local' type=ecdsa curve=secp384r1

# generate a 4096-bit RSA certificate signing request (CSR)
- generate_ssl_cert: path=/etc/server.csr C=CA ST=Ontario L=Toronto O="ACME Corp." OU=IT CN=www.example.local email='security@example.local' self_signed=no key_size=4096

# generate a RSA self-signed certificate and use SHA512 as the hash algorithm
- generate_ssl_cert: path=/etc/server.csr CN=www.example.local digest=sha512

# generate a self-signed certificate using a custom openssl.cnf file and extensions
- generate_ssl_cert: path=/etc/server.csr CN='www.example.local' key_size=4096 cnf_file=/tmp/openssl.cnf extensions=custom
'''

RETURN = '''
ssl_type:
    description: the type of ssl certificate generated
    returned: changed
    type: string
    sample: "RSA"
ssl_cert:
    description: the remote absolute path to the created ssl certificate file
    returned: changed
    type: string
    sample: "/path/to/server.crt"
ssl_key:
    description: the remote absolute path to the created ssl private key file
    returned: changed
    type: string
    sample: "/path/to/server.key"
openssl_error:
    description: the error message returned by openssl
    returned: failed
    type: string
    sample: "problems making Certificate Request..."
'''

#---- Logic Start ------------------------------------------------------------#

# function to validate remote target path
def validate_destination(dest, module):
    dir_name = os.path.dirname(dest)
    if not os.path.exists(dir_name):
        try:
            os.stat(dir_name)
        except OSError, e:
            if "permission denied" in str(e).lower():
                module.fail_json(msg="Destination directory %s is not accessible." % (dir_name))
        try:
            os.makedirs(dir_name, 0700)
        except OSError, e:
            if "permission denied" in str(e).lower():
                module.fail_json(msg="You don't have permission to create destination directory: %s." % (dir_name))
    if not os.access(os.path.dirname(dest), os.W_OK):
        module.fail_json(msg="Destination %s is not writable." % (dir_name))    


# function to check for and backup existing ssl files
def backup_files(module, files):
    backup = module.params.get('backup', False)
    if backup:
        for filename in files:
            if os.path.exists(filename):
                module.backup_local(filename)
    else:
        if os.path.exists(files[0]):
            module.exit_json(changed=False, msg="SSL files already exist.")


# function to check if chosen hash algorithm is valid
def check_hash_algorithm(module, openssl_binary, digest):
    openssl_cmd = shlex.split(openssl_binary + ' list-message-digest-algorithms')
    result, stdout, stderr = module.run_command(openssl_cmd)
    if result != 0:
        module.fail_json(changed = False, msg="Failed to execute openssl command: %s." % ' '.join(openssl_cmd), openssl_error = stderr)    
    supported_digests = [ i.lower() for i in stdout.split('\n') if "=>" not in i ] 
    if digest in supported_digests:
        return True
    else:
        module.fail_json(msg="Chosen hash algorithm %s is not supported on the target server." % digest)


# function to check if chosen ecc curve is valid
def check_ecdsa_curve(module, openssl_binary, curve):
    openssl_cmd = shlex.split(openssl_binary + ' ecparam -list_curves')
    result, stdout, stderr = module.run_command(openssl_cmd)
    if result != 0:
        module.fail_json(changed = False, msg="Failed to execute openssl command: %s." % ' '.join(openssl_cmd), openssl_error = stderr) 
    supported_curves = [ i.split(':')[0].strip().lower() for i in stdout.split('\n') if "=>" not in i ]
    if curve in supported_curves:
        return True
    else:
        module.fail_json(msg="Chosen ECC curve %s is not supported on the target server." % curve)


def main():
    module = AnsibleModule(
        argument_spec = dict(
            dest      = dict(required=True, type='path', aliases=["path", "destfile"]),
            backup    = dict(required=False, type='bool', default=False),
            encrypt   = dict(required=False, type='bool', default=False),
            password  = dict(required=False, type='str', no_log=True, default=None),
            self_signed = dict(required=False, type='bool', default=True),
            type      = dict(required=False, default='rsa', choices=['rsa', 'ecdsa'], aliases=["cert_type"]),
            key_size  = dict(required=False, type='int', default=2048),
            curve     = dict(required=False, type='str', default=None),
            digest    = dict(required=False, type='str', default=None, aliases=["hash"]),
            days_valid= dict(required=False, type='int', default=365),
            country   = dict(required=False, type='str', aliases=["C"]),
            state     = dict(required=False, type='str', aliases=["ST"]),
            city      = dict(required=False, type='str', aliases=["L"]),
            org       = dict(required=False, type='str', aliases=["O"]),
            org_unit  = dict(required=False, type='str', aliases=["OU"]),
            common_name=dict(required=True, type='str', aliases=["CN"]),
            email     =dict(required=False, type='str', aliases=["email_address"]),
            cnf_file  = dict(required=False, type='str', default=None, aliases=["config", "config_file"]),
            extensions  = dict(required=False, type='str', default=None, aliases=["ext"]),
            ),
            required_if = [
                ('encrypt', True, ['password']),
                ('type', 'ecdsa', ['curve'])
            ],           
            add_file_common_args = True,
            supports_check_mode = False,
        )

    module.load_file_common_arguments(module.params)

    dest = os.path.expanduser(module.params['dest'])
    encrypt = module.params.get('encrypt', False)
    password = module.params.get('password', None)
    self_signed = module.params.get('self_signed', False)
    cert_type = module.params.get('type', 'rsa')
    key_size = module.params.get('key_size', 2048)
    curve = module.params.get('curve', None)
    digest = module.params.get('digest', None)
    days_valid = module.params.get('days_valid', 365)
    config_file = module.params.get('cnf_file', None)
    extensions = module.params.get('extensions', None)
    mode   = module.params.get('mode', None)
    owner  = module.params.get('owner', None)
    group  = module.params.get('group', None)

    country = '/C=' + str(module.params.get('country', None))
    state = '/ST=' + str(module.params.get('state', None))
    city = '/L=' + str(module.params.get('city', None))
    organization = '/O=' + str(module.params.get('org', None))
    organizational_unit = '/OU=' + str(module.params.get('org_unit', None))
    common_name = '/CN=' + module.params['common_name']
    email = '/emailAddress=' + str(module.params.get('email', None))

    validate_destination(dest, module)

    backup_files(module, [dest, os.path.splitext(dest)[0] + ".key"])

    if self_signed:
        cert_file = os.path.splitext(dest)[0] + ".crt"
    else:
        cert_file = os.path.splitext(dest)[0] + ".csr"
    key_file = os.path.splitext(dest)[0] + '.key'

    ssl_files = [cert_file, key_file]

    openssl_binary = module.get_bin_path("openssl")
    if openssl_binary is None:
        module.fail_json(msg="This module requires OpenSSL which was not found on the system.")

    subject_fields = [country, state, city, organization, organizational_unit, common_name, email]
    ssl_subject = "".join([field for field in subject_fields if not re.match('\/[A-Z]+=None', field, re.I)])

    openssl_args = " req -new -x509 -batch -nodes -newkey rsa:%s -keyout %s -out %s -days %s -subj '%s'" % (key_size, key_file, cert_file, days_valid, ssl_subject)
    openssl_cmd = shlex.split(openssl_binary + openssl_args)

    if not self_signed:
        openssl_cmd.remove('-x509')
        index_pos = openssl_cmd.index('-out')
        openssl_cmd[index_pos + 1] = cert_file
        ssl_files[0] = cert_file

    if encrypt:
        index_pos = openssl_cmd.index('-nodes')
        openssl_cmd[index_pos] = '-passout'
        openssl_cmd.insert(index_pos + 1, 'pass:%s' % password)

    if config_file is not None:
        if os.path.exists(config_file):
            openssl_cmd.append('-config')
            openssl_cmd.append(config_file)
            if extensions is not None:
                openssl_cmd.append('-extensions')
                openssl_cmd.append(extensions)
        else:
            module.fail_json(msg="Specified openssl.cnf file %s does not exist." % config_file)

    if digest is not None:
        digest = digest.lower()
        if check_hash_algorithm(module, openssl_binary, digest):
            openssl_cmd.append('-%s' % digest)

    if cert_type.lower() == 'ecdsa':
        curve = curve.lower()
        if check_ecdsa_curve(module, openssl_binary, curve):
            ecparam_file = os.path.dirname(os.path.abspath(__file__)) + '/ecparam'
            openssl_ecparam_cmd = openssl_binary + " ecparam -name %s -out %s" % (curve, ecparam_file)
            result, stdout, stderr = module.run_command(openssl_ecparam_cmd)
            if result != 0:
                module.fail_json(changed = False, msg="Failed to create EC parameter file.", openssl_error = stderr)
            else:
                index_pos = openssl_cmd.index('rsa:%s' % key_size)
                openssl_cmd[index_pos] = 'ec:%s' % ecparam_file          

    result, stdout, stderr = module.run_command(openssl_cmd)

    if result != 0:
        module.fail_json(changed = False, msg="Failed to create SSL certificate and key files.", openssl_error = stderr)
    else:
        module.set_mode_if_different(key_file, '0600', False)
        if mode is not None:
            module.set_mode_if_different(cert_file, mode, False)
        if owner is not None:
            for filename in ssl_files:
                module.set_owner_if_different(filename, owner, False)
        if group is not None:
            for filename in ssl_files:
                module.set_group_if_different(filename, group, False)
        module.exit_json(changed = True, result = "SSL certificate %s and key %s created successfully." % (cert_file, key_file), ssl_type=cert_type.upper(), ssl_cert=cert_file, ssl_key=key_file)


#---- Import Ansible Utilities (Ansible Framework) ---------------------------#

from ansible.module_utils.basic import *

if __name__ == '__main__':
    main()
