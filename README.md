# Ansible: Deploy OSSEC HIDS server!

[![Status](https://img.shields.io/badge/status-complete-brightgreen.svg?style=plastic)](#)
[![Platform](https://img.shields.io/badge/platform-CentOS%206-lightgrey.svg?style=plastic)](#)
[![GPLv3](https://img.shields.io/badge/license-GPLv3-blue.svg?style=plastic)](https://www.gnu.org/licenses/gpl.html)

This is an Ansible playbook that deploys [OSSEC HIDS](https://ossec.github.io/) on a CentOS 6 server as part of the ezLSM capstone project. This playbook should be run after the deploy-vault-server playbook. 

# Prerequisites

Ansible v2.0 is required to use this repo. To install Ansible, see [here](https://docs.ansible.com/ansible/intro_installation.html#installing-the-control-machine).

Two Vault tokens are required to obtain signed certificates from the Vault server for the ossec-hids and splunk-forwarder roles. The first Vault token is located in `/root/.server-certs-token.yml` and the second Vault token is located in `/root/.client-certs-token.yml` on the Vault server.

# Roles

This playbook consists of multiple roles:

1. common
    - installs basic packages required by later stages of the playbook
2. rsyslog
    - configures rsyslog to redirect security logs for comsumption by Splunk
3. iptables
    - configures host-based firewall rules
4. auditd
    - configures the audit subsystem
5. psad
    - installs and configures [psad](http://cipherdyne.org/psad/) to detect port scan and other attacks
6. ossec-server
    - installs [OSSEC HIDS](https://ossec.github.io/) 2.9.0 and configures it for automated agent enrollment
7. splunkforwarder
    - installs and configures a [Splunk forwarder](https://www.splunk.com/en_us/download/universal-forwarder.html) to collect and send local logs to the Splunk server
8. monit
    - installs and configures [monit](https://www.mmonit.com/monit/) for automated process monitoring

# Usage

## Deploying the playbook

1. clone this repo: 

    `$ git clone https://gitlab.com/ezLSM/deploy-ossec-server.git`

2. add the hostname or IP address of the remote server(s) to the file `inventory`
3. configure options in `group_vars/all` 
4. run the playbook as the root user: 

    `$ ansible-playbook site.yml -i inventory -u root -k -K`
    
## Expected Outcome

See build status for expected outcome of a successful run of this playbook. 
